package io.temkiiiiin.tpi;


import com.pi4j.io.gpio.*;

public final class ADCReader {

    private static GpioPinDigitalInput misoInput;
    private static GpioPinDigitalOutput mosiOutput;
    private static GpioPinDigitalOutput clockOutput;
    private static GpioPinDigitalOutput chipSelectOutput;

    // Note: "Mismatch" 23-24. The wiring says DOUT->#23, DIN->#24
    // 23: DOUT on the ADC is IN on the GPIO. ADC:Slave, GPIO:Master
    // 24: DIN on the ADC, OUT on the GPIO. Same reason as above.
    // SPI: Serial Peripheral Interface
    private static Pin spiClk = RaspiPin.GPIO_01; // Pin #18, clock
    private static Pin spiMiso = RaspiPin.GPIO_04; // Pin #23, data in.  MISO: Master In Slave Out
    private static Pin spiMosi = RaspiPin.GPIO_05; // Pin #24, data out. MOSI: Master Out Slave In
    private static Pin spiCs = RaspiPin.GPIO_06; // Pin #25, Chip Select

    public static int readAdc(GpioController gpio, int adcChannel) {
        if (misoInput == null) {
            misoInput = gpio.provisionDigitalInputPin(spiMiso, "MISO");
            mosiOutput = gpio.provisionDigitalOutputPin(spiMosi, "MOSI", PinState.LOW);
            clockOutput = gpio.provisionDigitalOutputPin(spiClk, "CLK", PinState.LOW);
            chipSelectOutput = gpio.provisionDigitalOutputPin(spiCs, "CS", PinState.LOW);
        }

        chipSelectOutput.high();

        clockOutput.low();
        chipSelectOutput.low();

        int adccommand = adcChannel;
        adccommand |= 0x18; // 0x18: 00011000
        adccommand <<= 3;
        // Send 5 bits: 8 - 3. 8 input channels on the MCP3008.
        for (int i = 0; i < 5; i++) //
        {
            if ((adccommand & 0x80) != 0x0) // 0x80 = 0&10000000
                mosiOutput.high();
            else
                mosiOutput.low();
            adccommand <<= 1;
            clockOutput.high();
            clockOutput.low();
        }

        int adcOut = 0;
        for (int i = 0; i < 12; i++) // Read in one empty bit, one null bit and 10 ADC bits
        {
            clockOutput.high();
            clockOutput.low();
            adcOut <<= 1;

            if (misoInput.isHigh()) {
                //      System.out.println("    " + misoInput.getName() + " is high (i:" + i + ")");
                // Shift one bit on the adcOut
                adcOut |= 0x1;
            }
        }
        chipSelectOutput.high();

        adcOut >>= 1; // Drop first bit
        return adcOut;
    }

}
