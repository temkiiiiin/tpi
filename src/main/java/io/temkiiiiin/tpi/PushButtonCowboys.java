package io.temkiiiiin.tpi;


import com.pi4j.io.gpio.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class PushButtonCowboys {

    private static class Player {

        private final GpioPinDigitalInput button;
        private final GpioPinDigitalOutput diode;

        public PinState getButtonState() {
            return button.getState();
        }

        public void setState(PinState state) {
            diode.setState(state);
        }

        public Player(final GpioPinDigitalInput button, final GpioPinDigitalOutput diode) {
            this.button = button;
            this.diode = diode;

            diode.setShutdownOptions(true, PinState.LOW);
        }

    }

    private static final GpioController gpio = GpioFactory.getInstance();
    private static List<Player> players;
    private static GpioPinDigitalOutput gameLed;
    private static GpioPinDigitalInput gameBtn;

    public static void main(String[] args) {
        //создаю игроков
        players = new ArrayList<Player>();
        players.add(new Player(
                gpio.provisionDigitalInputPin(RaspiPin.GPIO_00, "playerBtn"),
                gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, "playerLed")
        ));
        players.add(new Player(
                gpio.provisionDigitalInputPin(RaspiPin.GPIO_02, "playerBtn"),
                gpio.provisionDigitalOutputPin(RaspiPin.GPIO_03, "playerLed")
        ));

        //диод и кнопка рестарта
        gameLed = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_06, "gameLed");
        gameLed.setShutdownOptions(true, PinState.LOW);

        gameBtn = gpio.provisionDigitalInputPin(RaspiPin.GPIO_04, "gameBtn");

        while (true) {
            newGame();
        }
    }

    public static void newGame() {
        System.out.println("start");

        //жду
        Random rnd = new Random();
        try {
            Thread.sleep(rnd.nextInt(10 * 1000) + 5000);
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<Player> losers = new ArrayList<Player>();
        for (Player player: players) {
            if (player.getButtonState() == PinState.HIGH) {
                losers.add(player);
            }
        }

        boolean restart = false;
        if (losers.size() > 0) {
            //лузеры
            for (Player player: players) {
                if (!losers.contains(player)) {
                    player.setState(PinState.HIGH);
                }
            }
        } else {
            // играем
            gameLed.setState(PinState.HIGH);
            List<Player> winners = new ArrayList<Player>();

            while (winners.size() == 0 && !restart) {
                for (Player player: players) {
                    if (player.getButtonState() == PinState.HIGH) {
                        winners.add(player);
                    }
                }
                restart = gameBtn.getState() == PinState.HIGH;
            }

            if (!restart) {
                for (Player player : winners) {
                    player.setState(PinState.HIGH);
                }
            }
        }

        if (!restart) {
            while (gameBtn.getState() == PinState.LOW) {
            }
        }

        gameLed.setState(PinState.LOW);
        for (Player player: players) {
            player.setState(PinState.LOW);
        }
    }

}
