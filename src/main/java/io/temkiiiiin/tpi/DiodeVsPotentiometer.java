package io.temkiiiiin.tpi;


import com.pi4j.io.gpio.*;

public class DiodeVsPotentiometer {

    public static void main(String[] args) {
        try {
            System.out.println("start diode");

            final GpioController gpio = GpioFactory.getInstance();

            final GpioPinDigitalOutput diodeOut = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_00, "MyLED", PinState.LOW);
            //diodeOut.setMode(PinMode.ANALOG_OUTPUT);
            diodeOut.setShutdownOptions(true, PinState.LOW);

            int counter = 20;
            int prev = 0;
            while (true || counter > 0) {
                int state = ADCReader.readAdc(gpio,0);
                if (state != prev) {
                    prev = state;
                    counter--;
                    System.out.println(state);
                    diodeOut.setState(PinState.getState(state));
                }
            }

            gpio.shutdown();

            System.out.println("end diode");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
