package io.temkiiiiin.tpi;


import com.pi4j.io.gpio.*;

public class DiodeVsButton {

    public static void main(String[] args) {
        try {
            System.out.println("start diode");

            final GpioController gpio = GpioFactory.getInstance();

            final GpioPinDigitalOutput diodeOut = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, "MyLED", PinState.LOW);
            diodeOut.setShutdownOptions(true, PinState.LOW);

            final GpioPinDigitalInput buttonInput = gpio.provisionDigitalInputPin(RaspiPin.GPIO_00, "MyBTN");

            int counter = 0;
            boolean btnPressed = false;
            do {
                if (buttonInput.isHigh() && !btnPressed) {
                    diodeOut.toggle();
                    Thread.sleep(1000);
                    diodeOut.toggle();

                    counter++;
                } else {
                    btnPressed = !btnPressed;
                }
            } while (counter < 3);

            gpio.shutdown();

            System.out.println("end diode");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
