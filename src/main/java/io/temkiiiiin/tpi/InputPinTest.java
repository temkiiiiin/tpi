package io.temkiiiiin.tpi;


import com.pi4j.io.gpio.*;

public class InputPinTest {

    public static void main(String[] args) {
        try {
            final GpioController gpio = GpioFactory.getInstance();

            final GpioPinDigitalInput pin = gpio.provisionDigitalInputPin(RaspiPin.GPIO_01);

            for (int i = 0; i < 100; i++) {
                System.out.println(pin.getState());
                Thread.sleep(100);
            }

            gpio.shutdown();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
