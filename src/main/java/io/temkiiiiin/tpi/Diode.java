package io.temkiiiiin.tpi;

import com.pi4j.io.gpio.*;

public class Diode {

    public static void main(String[] args) {
        try {
            System.out.println("start diode");

            final GpioController gpio = GpioFactory.getInstance();

            final GpioPinDigitalOutput pin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, "MyLED", PinState.LOW);
            pin.setShutdownOptions(true, PinState.LOW);

            for (int i = 0; i < 10; i++) {
                pin.toggle();
                Thread.sleep(1000);
            }

            gpio.shutdown();

            System.out.println("end diode");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
