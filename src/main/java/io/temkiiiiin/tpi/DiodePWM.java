package io.temkiiiiin.tpi;

import com.pi4j.io.gpio.*;

import java.util.Scanner;

public class DiodePWM {

    private static int timeLow = 5;
    private static int timeHeight = 100;

    public static void main(String[] args) {
        System.out.println("start diode");

        final GpioController gpio = GpioFactory.getInstance();

        final GpioPinDigitalOutput pin = gpio.provisionDigitalOutputPin(RaspiPin.GPIO_01, "MyLED", PinState.LOW);
        pin.setShutdownOptions(true, PinState.LOW);

        try {
            Thread t = new Thread(new Runnable() {

                public void run() {
                    Scanner scanner = new Scanner(System.in);
                    int n = 0;
                    do {
                        System.out.print("input t1" + String.valueOf(timeHeight) + ": ");
                        n = scanner.nextInt();
                        if (n < 0) {
                            continue;
                        }
                        timeHeight = n;
                    } while (n >= 0);
                }

            });

            t.start();

            while (true) {
                pin.setState(PinState.LOW);
                Thread.sleep(timeLow);
                pin.setState(PinState.HIGH);
                Thread.sleep(timeHeight);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        gpio.shutdown();
        System.out.println("end diode");
    }

}
